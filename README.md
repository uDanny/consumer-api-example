# consumerApiExample

Spring service with kafka consumer role

# Info

App runs on 8081 port.
Use all defaults Kafka links (2181 for zookeeper and 9092 for broker).
All base configs could be found in application.yaml
Support file formats: .mp3, .jpg, .png

Communication through Kafka DTOs: KafkaTextMessageDto, KafkaFileMessageDto.

[Producer project](https://gitlab.com/uDanny/assistantapi)


## Kafka running on one node
```bash
zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties

kafka-server-start /usr/local/etc/kafka/server.properties

kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic textTopic

kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic fileTopic

```

## Installation

```bash
mvn clean install
mvn spring-boot:run
```
