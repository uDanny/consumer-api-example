package com.utm.api.consumer;

import com.utm.api.consumer.consumer.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class ConsumerApplication {
    @Autowired
    ConsumerService consumerService;

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runConsumer() {
        consumerService.runFileConsumer();
    }
    @EventListener(ApplicationReadyEvent.class)
    public void runConsumer2() {
        consumerService.runTextConsumer();
    }

}
