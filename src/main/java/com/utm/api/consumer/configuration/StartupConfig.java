package com.utm.api.consumer.configuration;

import com.fasterxml.jackson.databind.JsonNode;
import com.utm.api.consumer.consumer.ConsumerBuilder;
import com.utm.api.consumer.dto.KafkaTextMessageDto;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StartupConfig {

    @Autowired
    private ConsumerBuilder consumerBuilder;

    @Bean
    KafkaConsumer<String, JsonNode> textKafkaConsumer() {
        return consumerBuilder.getTextConsumer();
    }

    @Bean
    KafkaConsumer<String, JsonNode> fileKafkaConsumer() {
        return consumerBuilder.getFileConsumer();
    }

}
