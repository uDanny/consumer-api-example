package com.utm.api.consumer.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.utm.api.consumer.dto.KafkaTextMessageDto;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Properties;

@Service
public class ConsumerBuilder {

    @Value("${kafka.brokers}")
    String brokers;
    @Value("${kafka.text-topic-name}")
    String textTopic;
    @Value("${kafka.file-topic-name}")
    String fileTopic;
    @Value("${kafka.group-id-config}")
    String group_id_config;

    private Properties props = new Properties();

    @PostConstruct
    private void propertiesInit(){
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group_id_config);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class.getName());
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 200);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);

        props.put("fetch.min.bytes", 1);
        props.put("heartbeat.interval.ms", 3000);
        props.put("max.partition.fetch.bytes", 1048576);
        props.put("session.timeout.ms", 30000);
        props.put("auto.offset.reset", "latest");
        props.put("connections.max.idle.ms", 540000);
        props.put("exclude.internal.topics", true);
        props.put("partition.assignment.strategy", "org.apache.kafka.clients.consumer.RangeAssignor");
        props.put("request.timeout.ms", 40000);
        props.put("auto.commit.interval.ms", 5000);
        props.put("fetch.max.wait.ms", 500);
        props.put("metadata.max.age.ms", 300000);
        props.put("reconnect.backoff.ms", 50);
        props.put("retry.backoff.ms", 100);
        props.put("client.id", "");
    }

    public KafkaConsumer<String, JsonNode> getTextConsumer() {
        KafkaConsumer<String, JsonNode> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(textTopic));
        return consumer;
    }

    public KafkaConsumer<String, JsonNode> getFileConsumer() {
        KafkaConsumer<String, JsonNode> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(fileTopic));
        return consumer;
    }
}
