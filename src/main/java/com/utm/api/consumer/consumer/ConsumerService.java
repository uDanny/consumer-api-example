package com.utm.api.consumer.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utm.api.consumer.dto.KafkaFileMessageDto;
import com.utm.api.consumer.dto.KafkaTextMessageDto;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Duration;

@Service
@EnableAsync
public class ConsumerService {

    private final KafkaConsumer<String, JsonNode> textConsumer;
    private final KafkaConsumer<String, JsonNode> fileConsumer;
    private final ObjectMapper objectMapper = new ObjectMapper();


    @Autowired
    public ConsumerService(@Qualifier("textKafkaConsumer") KafkaConsumer<String, JsonNode> textConsumer,
                           @Qualifier("fileKafkaConsumer") KafkaConsumer<String, JsonNode> fileConsumer) {
        this.textConsumer = textConsumer;
        this.fileConsumer = fileConsumer;
    }


    @Async
    public void runTextConsumer() {
        while (true) {
            ConsumerRecords<String, JsonNode> consumerRecords = textConsumer.poll(Duration.ofSeconds(10));
            consumerRecords.forEach(record -> {
                System.out.println("Record Key " + record.key());
                System.out.println("Record value " + record.value().asText());
                System.out.println("Record partition " + record.partition());
                System.out.println("Record offset " + record.offset());
                try {
                    System.out.println("Object: " + objectMapper.treeToValue(record.value(), KafkaTextMessageDto.class));
                } catch (JsonProcessingException e) {
                    System.out.println("Serialization error " + e);
                }
            });
        }

    }

    @Async
    public void runFileConsumer() {
        while (true) {
            ConsumerRecords<String, JsonNode> consumerRecords = fileConsumer.poll(Duration.ofSeconds(11));
            consumerRecords.forEach(record -> {
                System.out.println("Record Key " + record.key());
                System.out.println("Record value " + record.value().asText());
                System.out.println("Record partition " + record.partition());
                System.out.println("Record offset " + record.offset());

                try {
                    System.out.println("Object toString: " + objectMapper.treeToValue(record.value(), KafkaFileMessageDto.class));
                } catch (JsonProcessingException e) {
                    System.out.println("Serialization error " + e);
                }

                try {

                    byte[] bytes = record.value().get("file").binaryValue();
                    Files.write(new File("/Users/danielursachi/Desktop/kafkaConsumer/a.jpg").toPath(), bytes);

                } catch (IOException e) {
                    System.out.println("FileReading error " + e);
                }
            });
        }
    }
}
