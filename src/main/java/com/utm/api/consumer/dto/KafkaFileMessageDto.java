package com.utm.api.consumer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class KafkaFileMessageDto implements Serializable {
    private String id;
    private String clientId;
    @ToString.Exclude
    private byte[] file;
    private MessageTypeEnum messageType;
}
