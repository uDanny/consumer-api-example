package com.utm.api.consumer.dto;

import lombok.ToString;

@ToString
public enum MessageTypeEnum {
    IMAGE,
    VOICE
}
