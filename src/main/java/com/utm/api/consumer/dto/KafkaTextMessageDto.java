package com.utm.api.consumer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaTextMessageDto {
    private String clientId;
    private String message;
    private String id;
}
